// ==UserScript==
// @name        Fetch test
// @include     http://google.com.au/505
//
// @updateURL   https://gitlab.com/Lancelovereading/Userscript/raw/master/Userscript-test.user.js
// @run-at      document-end
// @include-jquery    false
// @use-greasemonkey    true
// @require     https://cdn.jsdelivr.net/npm/vue
// @version     2018.6.6
// ==/UserScript==

document.head.innerHTML = function(){/*
  <meta name="viewport" content="width=device-width, initial-scale=1.0">'
  <style type="text/css">
  * {color: #60AA60; background-color: #39383D;}
  .v-enter-active, .v-leave-active {transition: opacity .3s}
  .v-enter, .v-leave-to {opacity: 0;}
  .spinner {
    text-align: left;
  }

  .spinner > div {
    width: 10px;
    height: 10px;
    background-color: #60AA60;

    border-radius: 100%;
    display: inline-block;
    -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both;
    animation: sk-bouncedelay 1.4s infinite ease-in-out both;
  }

  .spinner .bounce1 {
    -webkit-animation-delay: -0.32s;
    animation-delay: -0.32s;
  }

  .spinner .bounce2 {
    -webkit-animation-delay: -0.16s;
    animation-delay: -0.16s;
  }

  @-webkit-keyframes sk-bouncedelay {
    0%, 80%, 100% { -webkit-transform: scale(0) }
    40% { -webkit-transform: scale(1.0) }
  }

  @keyframes sk-bouncedelay {
    0%, 80%, 100% { 
      -webkit-transform: scale(0);
      transform: scale(0);
    } 40% { 
      -webkit-transform: scale(1.0);
      transform: scale(1.0);
    }
  }
  </style>
  */}.toString().slice(14,-3)
document.body.innerHTML = function(){/*
  <script src="https://cdn.jsdelivr.net/npm/vue"></script>
  <div id="app">
  		<p></p>
      <div style="display: inline-block;">本页URL: </div>
      <transition mode="out-in" style="display: inline-block;">
        <div v-if="loader" key="loaderkey" style="display: inline-block;">
        	<div style="display: inline-block;">Loading</div>
          <div class="spinner" style="display: inline-block;">
              <div class="bounce1"></div>
              <div class="bounce2"></div>
              <div class="bounce3"></div>
          </div>
        </div>
      	<div v-else key="urlkey" style="display: inline-block;">{{url}}</div>
      </transition>
  		<p></p>
      <div>
      <select v-model="domain" v-on:change="template">
        <option value="https://www.lewen8.com/">lewen8</option>
        <option value="https://www.sto.cc/">Sto</option>
        <option value="http://www.xbiquge.cc/">笔趣阁</option>
      </select>
      <input v-model="domain"><input v-model="book"><input v-model="page">
      <p></p>
      <input v-model="maincontent"><input v-model="textsplit"><input v-model="encoding">
      <p>{{urlplus}}</p>
      </div>
      <p>
          <button @click="previous">上一页</button>
          <button @click="update">转跳</button>
          <button @click="next">下一页</button>
      </p>
      <div><p v-for="data in datas">{{ data }}</p></div>
      <button @click="previous">上一页</button><button @click="next">下一页</button>
  </div>
  */}.toString().slice(14,-3)

new Vue({
  el: '#app',
  data: {
    datas: [],
    domain: '',
    book: '',
    page: '',
    url: '',
    loader: true,
    maincontent: '',
    textsplit: '',
    encoding: '',
    next_page: '',
    previous_page: '',
  },
  created () {
    this.update()
  },
  computed:{
    urlplus: function () {return this.get_url()}
  },
  methods: {
    template: function (_template) {
      if (this.domain == 'https://www.lewen8.com/') {
        this.book = 'lw26280/'; this.page = '1441389'; this.maincontent = 'content'; this.textsplit = "　　"; this.encoding = 'utf-8';
      } else if (this.domain == 'https://www.sto.cc/') {
        this.book = 'book-110585-'; this.page = '1'; this.maincontent = 'BookContent'; this.textsplit = "　　"; this.encoding = 'utf-8';
      } else if (this.domain == 'http://www.xbiquge.cc/') {
        this.book = 'book/10137/'; this.page = '7250084'; this.maincontent = 'content'; this.textsplit = "\n\n"; this.encoding = 'GBK';
      }
      this.update()
    },
    get_url: function () {
      return this.domain+this.book+this.page+'.html'
    },
    update: function () {
      this.loader = true;
      this.url = this.get_url();
      fetch (this.url)
        // .then(response => response.text())
        .then(response => response.blob())
        .then(blob => {
          var reader = new FileReader();
          reader.readAsText(blob, this.encoding)
          reader.onload = (() => {
            var text = reader.result;
            parser = new DOMParser();
            text = parser.parseFromString(text, "text/html");
            this.datas = text.getElementById(this.maincontent).textContent.split(this.textsplit);
            this.loader = false;
            console.log(this.loader);    
            // console.log(JSON.stringify(this.datas = text.getElementById(this.maincontent).textContent))
            this.next_page = text.querySelector('.bottem > a:nth-child(4)').getAttribute('href');
            this.previous_page = text.querySelector('.bottem > a:nth-child(2)').getAttribute('href');
          });
        })
        // .then(() => {this.loader = !this.loader;})
      window.scrollTo(0, 0);
    },
    next: function () {
      if (this.domain == 'http://www.xbiquge.cc/') {this.page = this.next_page.split("/").pop().slice(0,-5)}
      else {this.page = (Number(this.page)+1).toString()};
      this.update()
    },
    previous: function () {
      if (this.domain == 'http://www.xbiquge.cc/') {this.page = this.previous_page.split("/").pop().slice(0,-5)}
      else if (Number(this.page) < 2) {this.page = '1'} else {
        this.page = (Number(this.page)-1).toString()}
      this.update()
    },
  }
})
  